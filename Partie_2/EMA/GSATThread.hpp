#ifndef GSATTHREAD_HPP 
#define GSATTHREAD_HPP 

/* --- Include --- */
#include <vector>
#include <thread>
#include <mutex>
#include "GSAT/GSAT.hpp"
#include "ControlBandit.hpp"

/* --- Constante --- */
#define DEFAULT_NB_THREAD 4

/* --- Structure --- */
typedef struct {
	int threadId;
	unsigned int nbIteration;
	unsigned int heuristicFill;
	unsigned int heuristicSolve;
	unsigned int nbSatisfiedClausesFill;
	unsigned int nbSatisfiedClausesSolve;
}GSATResult;

class GSATThread {
	public:
		GSATThread(int, int, char**);
		~GSATThread();

		void useEmaMethod(bool);
		void useEpsilonMethod(double, bool);

		bool solve();
		bool solve(bool);

		void end();
		void end(bool);
		bool isEnd();

		void printResult();

		/* --- Getter --- */
		inline unsigned int getNbVariables() {return gsat[0]->getNbVariables();}
		inline unsigned int getNbClauses() {return gsat[0]->getNbClauses();}

	private:
		ControlBandit cb;
		bool bEnd;
		std::mutex mEnd;
		double calcTime;
		int nbThread;
		std::vector<GSAT*> gsat;
		std::vector<double> time;
		std::vector<std::thread> threads;
		GSATResult result;

		void initBandit(bool);
		void runBandit(bool);

		void initThread(int, bool);
		void runThread(int, bool);
		bool calc(int, bool);
};

#endif