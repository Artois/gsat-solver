Le programme implémente le principe des bandits manchots avec une moyenne classique, et en utilisant ou non un epsilon greedy.

Les commandes du makefile sont :
	make : pour compiler le programme. L'exécutable généré se nommera GSATSolver.
	make clean : pour supprimer le fichier exécutable et les .o
	make run : pour lancer le programme
	make epsilon : pour lancer le programme avec un epsilon greedy à 0.2

Le programme se lance avec la commande suivante :
	./GSATSolver -i file.cnf [-t int] [-s] [-e double]

Les arguments sont les suivants :
	-i file.cnf : Le chemin vers le fichier cnf à résoudre
	-t int : Le nombre de threads à utiliser (optionnel, par défaut 4)
	-s : Active le mode silencieux seul la satisfaisabilité (ou non) est affiché (optionnel)
	-e double : Utilisation de l'epsilon greedy avec la valeur donné. La valeur doit être compirse entre 0 et 1 sinon la valeur utilisé est 0.1 (optionnel)

Exemple de commande (celle exécutée par `make epsilon`)  : 
	./GSATSolver -i ../../benchmarks/uf150/uf150-099.cnf -t 4 -e 0.2