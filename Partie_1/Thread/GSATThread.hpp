#ifndef GSATTHREAD_HPP 
#define GSATTHREAD_HPP 

/* --- Include --- */
#include <vector>
#include <thread>
#include <mutex>
#include "GSAT/GSAT.hpp"

/* --- Constante --- */
#define DEFAULT_NB_THREAD 4

/* --- Structure --- */
typedef struct {
	int threadId;
	unsigned int nbIteration;
	unsigned int heuristicFill;
	unsigned int heuristicSolve;
	unsigned int nbSatisfiedClausesFill;
	unsigned int nbSatisfiedClausesSolve;
}GSATResult;

class GSATThread {
	public:
		GSATThread(int, int, char**);
		~GSATThread();

		bool solve();
		bool solve(int);
		bool solve(bool);
		bool solve(int, bool);

		void end();
		void end(bool);
		bool isEnd();

		void printResult();

		/* --- Getter --- */
		inline unsigned int getNbVariables() {return gsat[0]->getNbVariables();}
		inline unsigned int getNbClauses() {return gsat[0]->getNbClauses();}

	private:
		bool bEnd;
		std::mutex mEnd;
		double calcTime;
		int nbThread;
		std::vector<GSAT*> gsat;
		std::vector<double> time;
		std::vector<std::thread> threads;
		int maxIteration;
		GSATResult result;

		void solverThread(int, bool);
};

#endif