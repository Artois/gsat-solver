Le programme implémente un solveur SAT en utilisant MPI.

Les commandes du makefile sont :
	make : pour compiler le programme. L'exécutable généré se nommera GSATSolver.
	make clean : pour supprimer le fichier exécutable et les .o
	make run : pour lancer le programme

Le programme s'utilise avec la commande suivante :
	mpirun -n int ./GSATSolver -i file.cnf

Les arguments sont les suivants :
	-n int : Le nombre de processus à utiliser
	-i file.cnf : Le chemin vers le fichier cnf à résoudre

Exemple de commande (celle exécutée par `make run`)  : 
	mpirun -n 4 ./GSATSolver -i ../../benchmarks/uf150/uf150-099.cnf