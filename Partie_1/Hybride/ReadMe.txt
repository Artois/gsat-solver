Le programme implémente un solveur SAT utilsant MPI et du multi threads.

Les commandes du makefile sont :
	make : pour compiler le programme. L'exécutable généré se nommera GSATSolver.
	make clean : pour supprimer le fichier exécutable et les .o
	make run : pour lancer le programme

Le programme s'utilise avec la commande suivante :
	mpirun -n int ./GSATSolver -i file.cnf [-t int]

Les arguments sont les suivants :
	-n int : Le nombre de processus à utiliser
	-i file.cnf : Le chemin vers le fichier cnf à résoudre
	-t int : Le nombre de threads à utiliser (optionnel, par défaut 4)

Exemple de commande (celle exécutée par `make run`)  : 
	mpirun -n 3 ./GSATSolver -i ../../benchmarks/uf150/uf150-099.cnf -t 2