Le programme implémente un solveur SAT en multi threads.

Les commandes du makefile sont :
	make : pour compiler le programme. L'exécutable généré se nommera GSATSolver.
	make clean : pour supprimer le fichier exécutable et les .o
	make run : pour lancer le programme

Le programme s'utilise avec la commande suivante :
	./GSATSolver -i file.cnf [-t int] [-s] [-m int]

Les arguments sont les suivants :
  	-i file.cnf : Le chemin vers le fichier cnf à résoudre
  	-t int : Le nombre de threads à utiliser (optionnel, par défaut 4)
  	-s : Active le mode silencieux seul la satisfaisabilité (ou non) est affiché (optionnel)
  	-m int : Le nombre maximum d'itération avant l'arret du programme (optionnel)

Exemple de commande (celle exécutée par `make run`)  : 
	./GSATSolver -i ../../benchmarks/uf150/uf150-099.cnf -t 4