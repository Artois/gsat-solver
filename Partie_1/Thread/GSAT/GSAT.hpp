
#ifndef GSAT_HPP_
#define GSAT_HPP_


#include "FillMethod.hpp"
#include "ArrayFiller.hpp"
#include "CFormula.hpp"
#include <iostream>
#include <string.h>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <set>
#include <string> 
#include <fstream>
#include <random>
#include <math.h>
#include <sys/time.h>

#define DEFAULT_ITERATIONS 1000
#define DEFAULT_MAX_TRY 255
#define DEFAULT_GREEDY_FACTOR 0.8
#define DEFAULT_GW_FACTOR 0.5
#define DEFAULT_RND_METHOD -1
#define DEFAULT_H_METHOD -1

class Options{
public:
  int iterations;
  int maxTry;
  float greedyFactor;
  float gwFactor;
  int rndMethod;
  int hMethod;
  char* filename;
};

char* getCmdOption(char ** begin, char ** end, const std::string & option);
bool cmdOptionExists(char** begin, char** end, const std::string& option);
void printHelp();

class GSAT{
public:
  CFormula* formula;
  Options* options;

  Options* setParameters(int argc,char* argv[]);
  void initialize();
  bool start(int fill=DEFAULT_RND_METHOD,int heuristic=DEFAULT_H_METHOD);
  inline double realTime() {
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return (double)tv.tv_sec + (double) tv.tv_usec / 1000000;
  }

  inline unsigned int getNbClauses(){return formula->getNbClauses();}
  inline unsigned int getNbVariables(){return formula->getNbVariables();}

  inline unsigned int getNbSatisfiedClausesFill(){return formula->getNbSatisfiedClausesFill();}
  inline unsigned int getNbSatisfiedClausesSolve(){return formula->getNbSatisfiedClausesSolve();}
  
  inline unsigned int getHeuristicFill(){return formula->getHeuristicFill();}
  inline unsigned int getHeuristicSolve(){return formula->getHeuristicSolve();}
  inline unsigned int getNbIterations(){return formula->getNbIterations();}
};



#endif
