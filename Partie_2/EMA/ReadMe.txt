Le programme implémente le principe des bandits manchots avec une moyenne exponentielle glissante, et en utilisant ou non un epsilon greedy. 
Par défaut le programme se lance avec un alpha statique (dont la valeur est 0.5).

Les commandes du makefile sont :
	make : pour compiler le programme. L'exécutable généré se nommera GSATSolver.
	make clean : pour supprimer le fichier exécutable et les .o
	make run : pour lancer le programme
	make epsilon : pour lancer le programme avec un epsilon greedy à 0.2
	make dynamic : pour lancer le programme avec un alpha dynamique sans epsilon greedy
	make start : pour lancer le programme avec un alpha dynamique avec epsilon greedy à 0.2

Le programme se lance avec la commande suivante :
	./GSATSolver -i file.cnf [-t int] [-s] [-e double] [-d]

Les arguments sont les suivants :
	-i file.cnf : Le chemin vers le fichier cnf à résoudre
	-t int : Le nombre de threads à utiliser (optionnel, par défaut 4)
	-s : Active le mode silencieux seul la satisfaisabilité (ou non) est affiché (optionnel)
	-e double : Utilisation de l'epsilon greedy avec la valeur donné. La valeur doit être compirse entre 0 et 1 sinon la valeur utilisé est 0.1 (optionnel)
	-d : Utilisation d'un alpha dynamique qui évolue entre 0.4 et 0.6 (optionnel)

Exemple de commande (celle éxécuté par `make start`)  : 
	./GSATSolver -i ../../benchmarks/uf150/uf150-099.cnf -t 4 -d -e 0.2