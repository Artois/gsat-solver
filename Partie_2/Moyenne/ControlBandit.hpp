#ifndef CONTROLBANDIT_HPP
#define CONTROLBANDIT_HPP 

/* --- Include --- */
#include <mutex>
#include <vector>
#include <stdlib.h>

/* --- Define --- */
#define NB_FILL 4
#define NB_HEURISTIC 7
#define MT_AVG 0
#define MT_EPS 1
#define EPSILON_DEFAULT 10

/* --- Structure --- */
typedef struct {
	int fill;
	int heuristic;
}fillAndHeuristic;

class ControlBandit {
	public:
		ControlBandit();
		~ControlBandit();

		void setMethod(int);
		void setEpsilon(double);

		bool queueIsEmpty();
		fillAndHeuristic next();

		void addFill(int, int);
		void addHeuristic(int, int);

	private:
		int method;
		int epsilon;
		std::mutex mutex;
		std::mutex mutexFill;
		std::mutex mutexHeuristic;
		std::vector<fillAndHeuristic> queue;
		double favg[NB_FILL];
		unsigned int fnb[NB_FILL];
		double havg[NB_HEURISTIC];
		unsigned int hnb[NB_HEURISTIC];

		fillAndHeuristic methodAvg();
		fillAndHeuristic methodEps();

		int getBestFill();
		int getBestHeuristic();
		inline int getRandomFill() {return rand() % 4 + 2;}
		inline int getRandomHeuristic() {return rand() % 7;}

};

#endif