#ifndef MAIN_HPP
#define MAIN_CPP

#include "GSATHybride.hpp"

extern int world_rank;
extern int world_size;

void help(char*);
void mpiWait(GSATThreadMPI*);
void mpiNotify(int);

#endif