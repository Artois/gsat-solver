#include "GSAT/GSAT.hpp"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <mpi.h>
#include "Main.hpp"

using namespace std;

int world_size;
int world_rank;

void help(char* prog) {
    fprintf(stderr, "usage: mpirun -n int ./%s -i file.cnf [-t int]\n\t-n Number of process to use\n\t-i CNF file to solve\n\t-t Number of threads to use\n", prog);
    exit(1);
}

int main(int argc, char* argv[]) {
    extern char * optarg; 
    int nbThread = DEFAULT_NB_THREAD;
    int opt;
    bool optI = false;

    char** gsatArg = (char**) malloc(sizeof(char*) * 3);
    gsatArg[0] = (char*) malloc(sizeof(char) * strlen(argv[0]) + 1);
    memset(gsatArg[0], 0, strlen(argv[0]) + 1);
    strncpy(gsatArg[0], argv[0], strlen(argv[0]));

    while((opt = getopt(argc, argv, "i:t:")) != -1) {
        switch(opt) {
            case 'i':
                optI = true;
                gsatArg[1] = (char*) malloc(sizeof(char) * 3);
                memset(gsatArg[1], 0, 3);
                strncpy(gsatArg[1], "-i", 2);
                gsatArg[2] = (char*) malloc(sizeof(char) * strlen(optarg) + 1);
                memset(gsatArg[2], 0, strlen(optarg) + 1);
                strncpy(gsatArg[2], optarg, strlen(optarg));
                break;
            case 't':
                nbThread = atoi(optarg);
                if(nbThread < 1) {
                    nbThread = 1;
                }
                break;
            case '?':
                help(argv[0]); 
        }
    }

    if(!optI) {
        help(argv[0]);
    }

    //Init MPI
    int provided;
    MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &provided);
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
  
    GSATThreadMPI* gsat = new GSATThreadMPI(nbThread, 3, gsatArg);
    if(world_rank == 0) {
        printf("c nbProcess: %d\n", world_size);
        printf("c nbThreads: %d\n", nbThread);
        printf("c nbVariables: %d\n", gsat->getNbVariables());
        printf("c nbClauses: %d\n", gsat->getNbClauses());
    }
    gsat->solve();

    //Le processus d'affichage attend de recevoir les resultats à afficher
    if(world_rank == DISPLAY_RANK) {
        sleep(1); //Attend la fin des autres processus
        //Recup les infos
        int pid, rank, thread;
        double time;
        unsigned int nbIte, heuriFill, heuriSolve, satisfFill, satisfSolve;
        MPI_Recv(&pid, 1, MPI_INT, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        MPI_Recv(&rank, 1, MPI_INT, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        MPI_Recv(&thread, 1, MPI_INT, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        MPI_Recv(&time, 1, MPI_DOUBLE, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        MPI_Recv(&nbIte, 1, MPI_UNSIGNED, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        MPI_Recv(&heuriFill, 1, MPI_UNSIGNED, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        MPI_Recv(&heuriSolve, 1, MPI_UNSIGNED, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        MPI_Recv(&satisfFill, 1, MPI_UNSIGNED, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        MPI_Recv(&satisfSolve, 1, MPI_UNSIGNED, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        //Set le resultat
        gsat->setResult(true, pid, rank, thread, time, nbIte, heuriFill, heuriSolve, satisfFill, satisfSolve);
        //Affiche resultat
        printf("-----------------------------------------------------------------------------------------------------------------\n");
        gsat->printResult();
    }
    
    MPI_Finalize();

}