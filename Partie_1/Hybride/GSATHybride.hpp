#ifndef GSATHYBRIDE_HPP 
#define GSATHYBRIDE_HPP 

/* --- Include --- */
#include <vector>
#include <thread>
#include <mutex>
#include "GSAT/GSAT.hpp"

/* --- Constante --- */
#define DEFAULT_NB_THREAD 4
#define DISPLAY_RANK 0

/* --- Structure --- */
typedef struct {
	int pid;
	int rank;
	int threadId;
	double calcTime;
	unsigned int nbIteration;
	unsigned int heuristicFill;
	unsigned int heuristicSolve;
	unsigned int nbSatisfiedClausesFill;
	unsigned int nbSatisfiedClausesSolve;
}GSATResult;

class GSATThreadMPI {
	public:
		GSATThreadMPI(int, int, char**);
		~GSATThreadMPI();

		bool solve();

		void end();
		void end(bool);
		bool isEnd();

		void printResult();

		/* --- Getter --- */
		inline unsigned int getNbVariables() {return gsat[0]->getNbVariables();}
		inline unsigned int getNbClauses() {return gsat[0]->getNbClauses();}
		inline GSATResult getResult() {return result;}
		void setResult(bool, int, int, int, double, unsigned int, unsigned int, unsigned int, unsigned int, unsigned int);

	private:
		bool bEnd;
		std::mutex mEnd;
		bool find;
		int nbThread;
		std::vector<GSAT*> gsat;
		std::vector<std::thread> threads;
		std::thread mpiSync;
		GSATResult result;
		
		void solverThread(int);
		void mpiWait(GSATThreadMPI*);
		void mpiNotify(int);
		void mpiSendResult(int);
};

#endif